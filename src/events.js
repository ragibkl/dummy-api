const fs = require('fs');
const pify = require('pify');

const fsPromise = pify(fs);

async function savePayload(timestamp, payload) {
  const file = `req_${timestamp}.json`;
  const content = JSON.stringify(payload, undefined, 2);

  await fsPromise.appendFile(file, content);
}

async function postEvents(req, res) {
  const { body, headers, query } = req
  const timestamp = new Date().toISOString()

  console.log({ timestamp, headers })
  await savePayload(timestamp, { timestamp, query, headers, body })
  res.send({ message: 'OK' })
}

module.exports = { postEvents }
