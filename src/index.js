const express = require('express')
const { postEvents } = require('./events')
const { getHealth } = require('./health')

const app = express()
const port = 80

app.use(express.json({ limit: '256mb' }))
app.get('/health', getHealth)
app.post('/events', postEvents)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
