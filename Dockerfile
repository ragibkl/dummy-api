FROM node:14-alpine

COPY src/ /node_app/src
COPY package.json /node_app/package.json
COPY yarn.lock /node_app/yarn.lock

WORKDIR /node_app

RUN yarn install --production
ENV NODE_ENV=production
CMD node src/index.js
